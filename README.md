# plug

![Hex.pm](https://img.shields.io/hexpm/dw/plug)
A specification and conveniences for composable modules between web applications. [plug](https://hex.pm/packages/plug)

* [Elixir School (lessons)](https://elixirschool.com/en/lessons/misc/plug)
